import { MdDashboard,MdPeople,MdQuestionAnswer,MdAssignmentLate } from "react-icons/md";


const Navbar = () => {


    return (
        <nav className="navbar">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <a href="#" className="nav-link">
                        <span className="link-icon"><MdDashboard /></span>
                        <span className="link-text">home</span>
                    </a>
                </li>

                <li className="nav-item">
                    <a href="#" className="nav-link">
                        <span className="link-icon"><MdAssignmentLate /></span>
                        <span className="link-text">questions</span>
                    </a>
                </li>

                <li className="nav-item">
                    <a href="#" className="nav-link">
                        <span className="link-icon"><MdQuestionAnswer /></span>
                        <span className="link-text">results</span>
                    </a>
                </li>

                <li className="nav-item">
                    <a href="#" className="nav-link">
                        <span className="link-icon"><MdPeople /></span>
                        <span className="link-text">contact</span>
                    </a>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar