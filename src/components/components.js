import Navbar from "./navbar";
import Slider from './slider';

const components = {
    Navbar,
    Slider,
}

export default components;