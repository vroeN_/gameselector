import './styles/styles.css'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Homepage from './pages/homepage';
import components from "./components/components"


function App() {
  const { Navbar, Slider } = components;

  return (
    <Router>
      <Navbar />
      <Slider />
      <Homepage />
    </Router>
  );
}

export default App;
